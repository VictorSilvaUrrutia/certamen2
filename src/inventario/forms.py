from django import forms
from .models import IngresoInventario



class RegForm(forms.Form):
    #registro_id = forms.AutoField(primary_key=True)
    nombre = forms.CharField(max_length=100)
    cantidad = forms.IntegerField()
    equipamiento = forms.CharField(max_length=100)
    hardware = forms.CharField(max_length=100)

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:  # Validamos que el contenido del campo nombre no venga vacío
            if len(nombre) <= 1:  # Verificamos que el contenido del campo nombre tiene un caracter
                raise forms.ValidationError("El campo nombre no puede ser de un caracter, NOMBRE NO VALIDO!")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")


class RegModelForm(forms.ModelForm):
    class Meta:
        modelo = IngresoInventario
        campos = ["nombre", "cantidad", "equipamiento", "hardware"]

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:  # Validamos que el contenido del campo nombre no venga vacío
            if len(nombre) <= 1:  # Verificamos que el contenido del campo nombre tiene un caracter
                raise forms.ValidationError("El campo nombre no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")
