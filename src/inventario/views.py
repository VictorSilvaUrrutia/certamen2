
from django.shortcuts import render
from .forms import RegForm
from .models import IngresoInventario

def inicio(request):
    form = RegForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        nombre2 = form_data.get("nombre")
        cantidad2 = form_data.get("cantidad")
        equipamiento2 = form_data.get("equipamiento")
        hardware2 = form_data.get("hardware")
        objeto = IngresoInventario.objects.create(nombre=nombre2, cantidad=cantidad2, equipamiento=equipamiento2, hardware=hardware2)


    contexto = {
        "el_formulario": form,
    }

    return render(request, "inicio.html", contexto)

