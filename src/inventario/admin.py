from django.contrib import admin
from .models import IngresoInventario
from .forms import RegForm, RegModelForm

class AdminIngresoInventario(admin.ModelAdmin):

    list_display = ["registro_id", "nombre", "cantidad", "equipamiento", "hardware"]
    form = RegModelForm
    list_filter = ["nombre","hardware"]
    search_fields = ["equipamiento", "nombre"]
    list_editable = ["hardware"]


#class Meta:
      # model = IngresoInventario

# Register your models here.

admin.site.register(IngresoInventario, AdminIngresoInventario)

