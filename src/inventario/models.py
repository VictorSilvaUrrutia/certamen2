from django.db import models

class IngresoInventario(models.Model):
    registro_id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100, blank=False, null=False)
    cantidad = models.IntegerField()
    equipamiento = models.CharField(max_length=100, null=True)
    hardware = models.CharField(max_length=100, null=True)



    def __str__(self):
        return self.nombre

# Create your models here.
