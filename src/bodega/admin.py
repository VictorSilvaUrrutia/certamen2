from django.contrib import admin
from .models import IngresoBodega


class AdminIngresoBodega(admin.ModelAdmin):
    list_display = ["registro_id", "nombre", "stock", "ubicacion"]
    #form = RegModelForm
    list_filter = ["nombre","ubicacion"]
    search_fields = ["ubicacion", "nombre"]
    list_editable = ["ubicacion"]

    class Meta:
       model = IngresoBodega


# Register your models here.
admin.site.register(IngresoBodega, AdminIngresoBodega)

