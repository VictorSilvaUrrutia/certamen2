from django.db import models


class IngresoBodega(models.Model):
    registro_id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100, blank=False, null=False)
    stock = models.IntegerField()
    ubicacion = models.CharField(max_length=100, null=True)
    fecha_ingreso = models.DateField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.nombre

# Create your models here.
